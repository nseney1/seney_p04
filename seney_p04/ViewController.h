//
//  ViewController.h
//  seney_p04
//
//  Created by Nicholas Ryan Seney on 3/13/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameView.h"

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet GameView *gameView;


@end

