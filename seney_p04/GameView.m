//
//  GameView.m
//  seney_p04
//
//  Created by Nicholas Ryan Seney on 3/13/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize player,platforms;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

bool direction = false;
int score = 0;

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        CGRect bounds = [self bounds];
        player = [[Player alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 100, 20, 20)];
        [player setBackgroundColor:[UIColor redColor]];
        [player setDx:0];
        [player setDy:10];
        [self addSubview:player];
    }
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mightwork.png"]];
    [self PlatformInit];
    return self;
}


-(void) PlatformInit
{
    CGRect bounds = [self bounds];
    int random = arc4random()%6;
    float ratio;
    float height;
    float width = 15;
    
    /*NSMutableArray *points;
    points = [[NSMutableArray alloc] init];
    CGPoint tempP;*/
  
    
    platforms = [[NSMutableArray alloc] init];
    Platforms *tempPlatform;
    if(platforms)
    {
        for(Platforms* tPlat in platforms)
        {
            [tPlat removeFromSuperview];
        }
    }
    while(random == 0)
        random = arc4random();
    
    CGPoint prevP;
    for(int i=0;i<4;i++)
    {
        random = arc4random()%5;
        if(random == 0 || random == 1)
            random = 2.0;
        ratio = 1.0/random;
        height = (bounds.size.height) * ratio;
        tempPlatform = [[Platforms alloc] initWithFrame:CGRectMake(0,0,width,height)];
        [tempPlatform setBackgroundColor:[UIColor greenColor]];
        [self addSubview:tempPlatform];
        
       [tempPlatform setCenter:(CGPointMake(arc4random()%(int)(bounds.size.width*.1),(arc4random()%(int)(bounds.size.height*.8))))];
        [platforms addObject:tempPlatform];
        
        
    }
}

-(void) makePlatform:(Platforms*)sender
{
   // [sender removeFromSuperview];
   // [platforms removeObject:sender];
    CGRect bounds = [super bounds];
    int random;
    int dx = arc4random()%(int)(bounds.size.width/4);
    float ratio;
    float height;
    float width = 15;
    random = arc4random()%3;
    ratio = 1.0/random;
    height = (bounds.size.height-10) * ratio;
    if(direction)
        [sender setCenter:(CGPointMake(dx,arc4random()%(int)bounds.size.height*.8))];
    else
        [sender setCenter:(CGPointMake(bounds.size.width-dx,arc4random()%(int)bounds.size.height*.8))];
    [self addSubview:sender];
    score++;
    [_highScoreLabel setText:([NSString stringWithFormat:@"Highscore: %d",score])];
    
}

-(void)animationStuff:(CADisplayLink *)sender
{
    CFTimeInterval ts = [sender timestamp];
    CGRect bounds = [self bounds];
    [player setDy:([player dy] + .3)];
    CGPoint p = ([player center]);
    p.y+= player.dy;
    if(p.y > bounds.size.height)
    {
        
            [self gameOver];
        
    }
    else if(p.y < 0)
    {
        p.y = 0;
    }
    for(Platforms *plats in platforms)
    {
        CGPoint b = [plats center];
        if(direction)
            b.x+= 5;
        else
            b.x-= 5;
        [plats setCenter:b];
        if(plats.center.x < 0)
        {
            [self makePlatform:plats];
        }
        else if(plats.center.x > bounds.size.width)
        {
            [self makePlatform:plats];
        }
    }
    for(Platforms *plats in platforms)
    {
        CGRect b = [plats frame];
        if(p.x > b.origin.x && p.x < b.origin.x + b.size.width && p.y > b.origin.y && p.y < b.origin.y + b.size.height)
        {
            if(direction)
                direction = false;
            else
                direction = true;
        }
    }
    [player setCenter:p];
}
-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    player.dy=-7;
}

-(IBAction)reset:(UIButton*)resetButton
{
    CGRect bounds = [self bounds];
    if(platforms)
    {
        for(Platforms* tPlat in platforms)
        {
            [tPlat removeFromSuperview];
        }
    }
    CGPoint p;
    p.x = bounds.size.width/2;
    p.y = bounds.size.height - 100;
    score = 0;
    [player setCenter:p];
    [player setDx:0];
    [player setDy:0];
    [_highScoreLabel setText:([NSString stringWithFormat:@"Highscore: %d",score])];
    direction = false;
    [_gameOverLabel setText:@""];
    [self PlatformInit];
}

-(void)gameOver
{
    [_gameOverLabel setText:@"GAME OVER!"];
    if(platforms)
    {
        for(Platforms* tPlat in platforms)
        {
            [tPlat removeFromSuperview];
            
        }
    }
    [platforms removeAllObjects];
}

@end
