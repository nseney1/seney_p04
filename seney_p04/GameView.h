//
//  GameView.h
//  seney_p04
//
//  Created by Nicholas Ryan Seney on 3/13/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"
#import "Platforms.h"

@interface GameView : UIView
@property(nonatomic,strong) NSMutableArray* platforms;
@property(nonatomic,strong) Player* player;
-(void)animationStuff:(CADisplayLink *)sender;
@property (strong, nonatomic) IBOutlet UILabel *highScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *gameOverLabel;
@property (strong,nonatomic) IBOutlet UIButton *resetButton;
@end
