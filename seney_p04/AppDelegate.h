//
//  AppDelegate.h
//  seney_p04
//
//  Created by Nicholas Ryan Seney on 3/13/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

